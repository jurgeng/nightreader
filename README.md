# Read And Wake Light

Using a Lolin32 (or basically any ESP), controlling a LED-strip, a 7-segment display (clock) and setting up a basic web server allowing to configure 3 time points: bedtime, alarm and earliest allowed wake-up time.
The only input is a touch contact.

# Functionality

## Bedtime
Prior to bedtime, the LED is configured to light up for 15 minutes when the contact is triggered in a slightly reddish hue.
After bedtime, the light will not be enough to read, but will light up 2 LED's for 2 minutes: just enough to remember where you were (nightmare) or go to the toilet.

## Light Alarm
When the alarm time is triggered, the lights will emit a fading in bright light (feature request: rainbow)
No snooze functionality, touching the trigger will turn off the light.

## Allowed to get up
I don't know about your kids, but if they have to get up early, you can't get them out of bed. But if the weekend comes around, they're rise and shining way too early.
Before the alarm time, the touch button will trigger the night lights to be red if getting up is not allowed, and green when getting up is okay.

## The time 
The time is fetched from an NTP server and synched every 48 hours and on power-up

## Settings
The Wemos is running a tiny web server. A web page grants access to the settings of the clock.
The following features are being implemented:
* Turn on/off the different clock settings
* Change the time of the three different parameters.
* Set each of the three values to "NOW"

# Hardware and associated libraries
* LED-strip with individually adresseable LED's. - using FastLED library
* 4 digit 7-segment LED display - using TM1637Display library (the Groove or just TM1637 libraries didn't work for me)
* Lolin32 with WiFi drivers - using espWiFi - don't use the default WiFi drivers, they won't find the hardware.
