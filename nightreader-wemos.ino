#include <FastLED.h>
#include <NTPClient.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <Arduino.h>
#include <TM1637Display.h>

// ------------------ USER PARAMETERS ------------------------

bool allNightLight = true;

byte bedtimeHour   = 23; // Hour   to switch to bedtime (lights go out)
byte bedtimeMinute = 30; // Minute to switch to bedtime (lights go out)
bool useBedTime    = true; // set false if there is no bedtime.

byte allowedHour   = 8;  // Hour   to allow getting up
byte allowedMinute = 0;  // Minute to allow getting up
bool useAllowed    = true; // set false if there is no earliest allowed time

byte wakeupHour    = 9;  // Hour   to start light wake up alarm
byte wakeupMinute  = 30; // Minute to start light wake up alarm
bool useWakeup     = true; // set false if there is no wake up alarm

int readingTime    = 1; // number of minutes to leave the light on

// Wifi settings
char* ssid     = "wifi";
char* password = "pass";


// Clock display
#define DISP_CLK 19
#define DISP_DIO 18
#define DISP_BRIGHTNESS 0x02

unsigned long hideClockMillis = 0;
unsigned int secondsToShowClock = 30 ; // How long should the clock be shown when button is pressed? (in seconds)
// ---------- TECHNICAL PARAMETERS -------------------------


// Debugging
bool debug = true;

// Debugging parameters
int speedUp  = debug ? 10 : 1;  // Speed up times with this factor (dividing the delays) - default = 1.
// CRGB startSleepRGB  = CRGB(0,255,255); // Very different colours to better see the difference (purple)
// CRGB endSleepRGB    = CRGB(255,255,0); // Very different colours to better see the difference (yellow)


TM1637Display display(DISP_CLK, DISP_DIO);

WiFiServer server(80);

// LED-strip
#define NUM_LEDS 30
#define DATA_PIN 17
#define BRIGHTNESS 64
CRGB startSleepRGB = CRGB(255, 241, 224) ; // 3200 Kelvin colour Temp (Halogen)
CRGB endSleepRGB   = CRGB(255, 147,  41) ; // 1900 Kelvin colour Temp (Candle)

CRGB leds[NUM_LEDS];

// Touch interface
#define TOUCHPIN T0
#define TOUCHTHRESHOLD 35
volatile byte lightsOn = 0;

// Time and time server
unsigned long epochDelta = 0; // Time difference between epoch and millis()
unsigned long lastUpdate = 0; // epoch for last NTP sync
const char* ntp_server = "europe.pool.ntp.org";
const unsigned int offset = 7200; // TimeZone offset in seconds;

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, ntp_server, offset);

// Gets triggered by the touch interrupt - keep short as possible. Has debouncing.
void buttonPressed() {
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupt comes faster than 100 ms, assume it's a bounce and ignore it
  if (interrupt_time - last_interrupt_time > 100)
  {
    lightsOn = lightsOn == 0 ? 1 : 0 ; // Toggle the value of lightsOn
    Serial.print("Button Pressed - clock start millis: ");
    hideClockMillis = interrupt_time + (secondsToShowClock*1000);
    Serial.println(hideClockMillis);
  }
  last_interrupt_time = interrupt_time;

}

// Function to start reading lamp mode (evening colours) - input in minutes
void nightRead(int readingTime) { // Reading time in minutes
  int secs = readingTime * 60;
  if (lightsOn) {
    for (int timer = secs; timer > 0; timer--) {
      // Serial.print("Now:"); Serial.println(timer);
      float coefficient = float(timer) / float(secs);
      //Serial.print("Coeff:"); Serial.println(coefficient);
      int Red   = startSleepRGB.r == endSleepRGB.r ? startSleepRGB.r : startSleepRGB.r + (endSleepRGB.r - startSleepRGB.r) * (1 - coefficient);
      //Serial.print("R:"); Serial.println(Red);
      int Green = startSleepRGB.g == endSleepRGB.g ? startSleepRGB.g : startSleepRGB.g + (endSleepRGB.g - startSleepRGB.g) * (1 - coefficient);
      //Serial.print("G:"); Serial.println(Green);
      int Blue  = startSleepRGB.b == endSleepRGB.b ? startSleepRGB.b : startSleepRGB.b + (endSleepRGB.b - startSleepRGB.b) * (1 - coefficient);
      //Serial.print("B:"); Serial.println(Blue);
      unsigned int soBrite = timer > secs * .2 ? BRIGHTNESS : BRIGHTNESS * (timer / (secs * .2));
      //Serial.print("secs:"); Serial.println(secs);
      //Serial.print("soBrite:"); Serial.println(soBrite);
      if (!lightsOn) {
        break;
      }
      // Show the leds
      FastLED.showColor(CRGB(Red, Green, Blue), soBrite);
      // Wait a little bit before we loop around and do it again
      delay(1000 / speedUp);
      // Serial.println("==============================");
    }
  }
  FastLED.clear();
  FastLED.show();
  lightsOn = false;
}

void showTime() {
  showTime(calcTime());
}

void showTime(int timeblock) {
  if(timeblock > 2359) {
    showTime(); // If an invalid value for timeblock is given, current time is fetched
  }
  display.setBrightness(0x00);
  display.showNumberDecEx(timeblock, 64, true);  
}

void showTime(int hh, int mm) {
  if(hh > 23 or mm > 59) {
    showTime(); // If an invalid value for HH or MM is given, current time is searched
  }
    showTime(hh * 100 + mm);
}

unsigned int calcTime() {
  unsigned long epochTime = epochDelta + (millis()/1000);
  // Strip days from epoch
  unsigned int hh = (epochTime  % 86400L) / 3600; // 24x60x60 = 86.400 -> seconds in a day. Remainder = number of seconds in today. Divide by 3600 to have number of hours into the day.
  unsigned int mm = (epochTime  % 3600) / 60; // 60x60 = 3600 -> seconds in an hour. Remainder = number of seconds in current hour. Divide by 60 to have number of minutes into the hour.
  return hh * 100 + mm;

}

// Checks if current time is past bedtime and sends "true" if it is.
bool pastBedtime() {
  // Strip days from epoch
  unsigned long secondsPerDay = 24 * 60 * 60;
  unsigned long secondsToday = ((epochDelta + millis() / 1000) % secondsPerDay);

  // Convert bedtime to seconds
  unsigned long secondsBedtime = bedtimeHour * 3600 + bedtimeMinute * 60;
  // Serial.print("Seconds for bedtime:");Serial.println(secondsBedtime);

  // It's past bedtime if ...
  return secondsBedtime < secondsToday;
}

// Activate 2 strong LED dots to give some extra light when waking up at night.
void smallLight(int timer) {
  FastLED.clear();
  leds[0] = endSleepRGB;
  leds[NUM_LEDS - 1] = endSleepRGB;
  FastLED.setBrightness(BRIGHTNESS);
  FastLED.show();
  delay(timer * 1000 / speedUp);
  FastLED.clear();
  FastLED.show();
  lightsOn = false;
}

// Turns on a few LED dots to supply in a night light.
void nightLight() {
  // TODO: change colour from reddish to blueish colour
  FastLED.clear();
  FastLED.show();
  if (allNightLight) {
    leds[1] = endSleepRGB;
    leds[NUM_LEDS - 2] = endSleepRGB;
    leds[NUM_LEDS / 2] = endSleepRGB;
    FastLED.setBrightness(BRIGHTNESS / 10);
    FastLED.show();
  }
}

// Connect to WiFi
void WiFiConnect() {
  WiFi.begin(ssid, password);
  Serial.println(WL_CONNECTED);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(WiFi.status());
    Serial.print(" - Retrying to connect to "); Serial.println(ssid);
    delay(500);
  }
  Serial.println("Connected to WiFi");
}

// Synchronizes the clock
void fixTimeDelta() {
  Serial.println("Fixing time");
  // Turn on maintenance light
  leds[7] = CRGB(95, 181, 205);
  FastLED.setBrightness(BRIGHTNESS / 10);
  FastLED.show();
  
  // Update Time Client
  timeClient.update();
  unsigned long epochTime = timeClient.getEpochTime();
  unsigned int hh = (epochTime  % 86400L) / 3600;
  unsigned int mm = (epochTime  % 3600) / 60;
  // parse variables
  lastUpdate = epochTime;
  epochDelta = epochTime - millis() / 1000;
  // Serial.println(timeClient.getFormattedTime());

  // TODO: Turn off WiFi chip

  // Turn of maintenance light
  leds[7] = CRGB(0, 0, 0);
  FastLED.show();
}

void displayWebpage(WiFiClient client) {
  // TODO Add Security Headers
  // TODO Implement security token in GET request

  String responseHeader = R"=====(
HTTP/1.1 200 OK
Content-type:text/html
Connection: close

)=====";
// Terminate with an extra newline
  String pageHeader = R"=====(
<!DOCTYPE html>
<html>
<head>
  <title>Slaaplamp GITTE</title>
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 28px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 20px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 4px;
}

.slider.round:before {
  border-radius: 5%;
}
</style>
  <script>
    function toggleTime() {
      var target = event.target || event.srcElement;
      var id = target.id;
      var field_id = id.split('_')[1];
      if(document.getElementById(id).checked) {
        document.getElementById(field_id).style.display = 'inline';
      } else {
        document.getElementById(field_id).style.display = 'none';
  document.getElementById(field_id).value="";
        var message = "?" + field_id + "=off";
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET",message, true);
        xhttp.send();
      }
    }

    function sendTime() {
      var xhttp = new XMLHttpRequest();
      var target = event.target || event.srcElement;
      var id = target.id;
      var value = document.getElementById(id).value;
      var message = "?" + id + "=" + value;
      xhttp.open("GET",message, true);
      xhttp.send();
    }

    function nowTime() {
      var today = new Date();
      sprintf(hour,"%02u", today.getHours());
      sprintf(minute,"%02u", today.getMinutes());
      var timestamp = hour + ":" + minute;
      var target = event.target || event.srcElement;
      var id = target.id;
      var field_id = id.split('_')[1];
      var checkbox_id = "use_" + field_id;
      if(!document.getElementById(checkbox_id).checked) {
        document.getElementById(checkbox_id).checked=true;
        document.getElementById(field_id).style.display = 'inline';
      }
      document.getElementById(field_id).value = timestamp;
      var message = "?" + field_id + "=" + timestamp;
      var xhttp = new XMLHttpRequest();
      xhttp.open("GET",message, true);
      xhttp.send();
    }

  </script>
</head>
)=====";

  String pageBody = R"=====(
<body>
  <h1>Stel slaaplamp GITTE in</h1>
  <form method="GET">
  <table>
  <tr>
    <td>Bedtijd:</td>
    <td><label class="switch"><input type="checkbox" id="use_bed" checked onchange="toggleTime()"><span class="slider round"></span></label></td>
    <td><input type="time" id="bed" onchange="sendTime()"></td>
    <td><button type="button" id="now_bed" onclick="nowTime()">NU</button></td>
  </tr>
  <tr>
    <td>Niet opstaan voor:</td>
    <td><label class="switch"><input type="checkbox" id="use_alw" checked onchange="toggleTime()"><span class="slider round"></span></label></td>
    <td><input type="time" id="alw" onchange="sendTime()"></td>
    <td><button type="button" id="now_alw" onclick="nowTime()">NU</button></td>
  </tr>
  <tr>
    <td>Wektijd:</td>
    <td><label class="switch"><input type="checkbox" id="use_alm" checked onchange="toggleTime()"><span class="slider round"></span></label></td>
    <td><input type="time" id="alm" onchange="sendTime()"></td>
    <td><button type="button" id="now_alm" onclick="nowTime()">NU</button></td>
  </tr>
  </table>
</form>
</body>
</html>


)=====";

  
  // HTML Headers

  Serial.println("Client connected");
  // Parsing header before sending out message
    client.println(responseHeader);
    client.println(pageHeader);         
    client.println(pageBody);         
}

void parseHeaders(WiFiClient client) {
  // TODO: only allow GET requests
  // TODO: validate using CSRF token in URL
  String request = client.readStringUntil('\r').substring(6);
  Serial.print("Request: "); Serial.println(request);
  client.flush();
  int whiteSpace = request.indexOf(" ");
  String cmnd = request.substring(0,3);
  String parm = request.substring(4, whiteSpace);
  // Known commands: alm - alw - bed
         if(cmnd == "bed") {
           Serial.print("Setting Bedtime to ");Serial.println(parm);
           if(parm == "off") {
            // TODO: Set bedtime trigger off
            useBedTime = false;
            Serial.println("Bedtime is turned off");
           } else {
            // TODO: Parse hour and minute
            useBedTime = true;
            int colon = parm.indexOf(":");
            // bedtimeHour   = parm.substring(0,colon).toInt(); // Hour   to switch to bedtime (lights go out)
            // bedtimeMinute = parm.substring(colon).toInt();   // Minute to switch to bedtime (lights go out)
            //Serial.print("Setting bedtime to");Serial.print(bedtimeHour);Serial.print(":");Serial.println(bedtimeMinute);
            Serial.println(parm.substring(0,colon).toInt()); // Hour   to switch to bedtime (lights go out)
            Serial.println(parm.substring(colon+1).toInt());   // Minute to switch to bedtime (lights go out)
           }
           

           // TODO: assign parm to correct parameters
  } else if(cmnd == "alw") {
           Serial.print("Setting Allowed wake up time to ");Serial.println(parm);
           // TODO: assign parm to correct parameters
    
  } else if(cmnd == "alm") {
           Serial.print("Setting Wake up alarm time to ");Serial.println(parm);
           // TODO: assign parm to correct parameters
    
  }
}
// ========================= MAIN FUNCTIONS ==========================

void setup() { 
  delay(1000); // Some extra time...
  touchAttachInterrupt(TOUCHPIN, buttonPressed, TOUCHTHRESHOLD);
	Serial.begin(115600);
	Serial.println("Initializing...");
	LEDS.addLeds<WS2812,DATA_PIN,GRB>(leds,NUM_LEDS);
	LEDS.setBrightness(BRIGHTNESS);

  WiFiConnect();

  // Base state
  FastLED.clear();
  FastLED.show();

  // Set Clock NTP + display
  fixTimeDelta();

  // Web Server...
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

void loop() {
  unsigned long startmillis = millis(); // will check for overflow at the end of the loop
  // Serial.print("lightsOn:"); Serial.println(int(lightsOn));
  // Serial.print("allNight:"); Serial.println(int(allNightLight));
  WiFiClient client = server.available();   // Listen for incoming clients
  if(client) {
    parseHeaders(client);
    displayWebpage(client);
  }

  // Check if it's past bedtime
  if(pastBedtime()) {
    // Serial.println("==> Past bedtime");
    if(allNightLight) nightLight();
    if(lightsOn) smallLight(30);
  } else {
    if(lightsOn) nightRead(15);  // Reading time in minutes
  }

  // Check if the button was pressed recently - then show the clock - else clear clock
  if (hideClockMillis > millis()) showTime(); else display.clear();

  // If invalid value for startmillis or epochDelta, get the right time...
  if (startmillis > millis() or epochDelta < 1500000000) fixTimeDelta();
  
  // Do a time check every 48 hours - allow 1/2 sec offset
  // 48 hours x 60 min x 60 sec x 1000 milli = 172800000
  if ((millis()%172800000) < 500) fixTimeDelta();

}
